from xbox360controller import Xbox360Controller
from visca_over_ip import Camera
import signal, time
from pynput.keyboard import Controller

keyboard = Controller()

currentIP = '192.168.5.20'
currentCam = Camera(currentIP)

def switchCam(axis):
    global currentIP
    global currentCam
    global keyboard
    currentCam.close_connection()
    if currentIP == '192.168.5.20':
        currentIP = '192.168.5.21'
    else:
        currentIP = '192.168.5.20'
    currentCam = Camera(currentIP)
    keyboard.type(" ")

def on_pantilt_moved(axis):
    # print('Axis {0} moved to {1} {2}'.format(axis.name, axis.x, axis.y))
    if abs(axis.x) < 0.2:
        axisx = 0
    else: 
        axisx = int(axis.x*24)
    if abs(axis.y) < 0.2:
        axisy = 0
    else: 
        axisy = int(axis.y*-24)

    # print('Pan: {0}. Tilt: {1}'.format(axisx, axisy))
    currentCam.pantilt(pan_speed=int(axisx), tilt_speed=int(axisy))
    
def on_zoom_moved(axis):
    print('Axis {0} moved to {1}'.format(axis.name, axis.value))
    currentCam.zoom(int(axis.value*7))


def on_dezoom_moved(axis):
    print('Axis {0} moved to {1}'.format(axis.name, axis.value))
    currentCam.zoom(int(axis.value*-7))

with Xbox360Controller(0, axis_threshold=0) as controller:
    while(1): 
    # try:
    
        # print("echo")
        # Button A events
        # controller.button_a.when_pressed = on_button_pressed
        # controller.button_a.when_released = on_button_released

        # Left and right axis move event
        controller.axis_l.when_moved = on_pantilt_moved
        controller.trigger_l.when_moved = on_dezoom_moved
        controller.trigger_r.when_moved = on_zoom_moved
        controller.button_trigger_l.when_pressed = switchCam
        # controller.axis_r.when_moved = on_axis_moved

    #         signal.pause()
    # except KeyboardInterrupt:
    #     pass
